// -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
// Example of a clock. This is very similar to the text-example,
// except that it shows the time :)
//
// This code is public domain
// (but note, that the led-matrix library this depends on is GPL v2)

#include "led-matrix.h"
#include "graphics.h"
#include "threaded-canvas-manipulator.h"
#include "pixel-mapper.h"

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <pthread.h>
#include <cmath>
#include <algorithm>    // std::find_if_not
#include "http_single_thread.cpp"

using namespace rgb_matrix;
RGBMatrix *matrix;
FrameCanvas *offscreen;

volatile bool interrupt_received = false;
static void InterruptHandler(int signo) {
  interrupt_received = true;
}


// Trim whitespace from the begining of the string
static inline std::string &ltrim(std::string &s)
{
  s.erase(s.begin(),std::find_if_not(s.begin(),s.end(),[](int c){return isspace(c);}));
  return s;
}

// Trim whitespace from the end of the string
static inline std::string &rtrim(std::string &s) {
  s.erase(std::find_if_not(s.rbegin(),s.rend(),[](int c){return isspace(c);}).base(), s.end());
  return s;
}

// Trim whitespace from the begining and end of the string
static inline std::string trim(const std::string &s) {
  std::string t=s;
  return ltrim(rtrim(t));
}

// Launch http thread for http control
void *http_thread_function(void *insettings) {
  struct LEDsettingsStruct *http_thread_settings =  (struct LEDsettingsStruct *) insettings;
  char doc_dir[] = "/home/pi/html/";
  httpserver serv(8080, doc_dir, http_thread_settings);
  serv.runserver();
  return 0;
}


std::string getconfigval(std::string file, std::string key) {
  std::string tempstring;
  std::ifstream configfile;
  std::string returnval = "";
  unsigned int pos;
  configfile.open(file, std::ifstream::in);
  while(!configfile.eof()) {
    std::getline(configfile, tempstring);
    if((pos = tempstring.find(key)) != std::string::npos) {
       trim(tempstring.substr(pos+key.length()+1));
    }
  }
  return "[notfound]";
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options]\n", progname);
  fprintf(stderr, "Reads text from stdin and displays it. "
          "Empty string: clear screen\n");
  fprintf(stderr, "Options:\n");
  rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-d <time-format>  : Default '%%H:%%M'. See strftime()\n"
          "\t-f <font-file>    : Use given font.\n"
          "\t-b <brightness>   : Sets brightness percent. Default: 100.\n"
          "\t-x <x-origin>     : X-Origin of displaying text (Default: 0)\n"
          "\t-y <y-origin>     : Y-Origin of displaying text (Default: 0)\n"
          "\t-S <spacing>      : Spacing pixels between letters (Default: 0)\n"
          "\t-C <r,g,b>        : Color. Default 255,255,0\n"
          "\t-B <r,g,b>        : Background-Color. Default 0,0,0\n"
          );

  return 1;
}

static bool parseColor(Color *c, const char *str) {
  return sscanf(str, "%hhu,%hhu,%hhu", &c->r, &c->g, &c->b) == 3;
}

static bool FullSaturation(const Color &c) {
    return (c.r == 0 || c.r == 255)
        && (c.g == 0 || c.g == 255)
        && (c.b == 0 || c.b == 255);
}

void FillSection(int x_start, int y_start, int x_end, int y_end, Color bg_color) {
  for(int y_temp = y_start;y_temp <= y_end && y_temp <= offscreen->height();y_temp++) {
    for(int x_temp  = x_start;x_temp < x_end && x_temp < offscreen->width();
          offscreen->SetPixel(x_temp++, y_temp, bg_color.r, bg_color.g, bg_color.b));
  }

}

int SetPixels(int pixels, Color paintcolor, int x, int y) {
  int i = 0;
  while(pixels & 4095) {
    if(pixels & 1) {
      offscreen->SetPixel(x, y, paintcolor.r, paintcolor.g, paintcolor.b);
    }
    i++;
    x++;
    if(x >= offscreen->width()) {
      x = 0;
      y++;
    }
    pixels = pixels >> 1;
  }
  return i;
}

void ScreenTransition() {
  for(int y_temp = 0;y_temp < offscreen->height();y_temp++) {
    for(int x_temp  = 0;x_temp < offscreen->width();
          offscreen->SetPixel(x_temp++, y_temp, rand() % 256, rand() % 256, rand() % 256), usleep(200));
    offscreen = matrix->SwapOnVSync(offscreen);
  }

}

int display_text(std::string text_buffer, rgb_matrix::Font *font, rgb_matrix::Font *outline_font,
                  Color outline_color, Color bg_color, Color color, int letter_spacing, int y, int x){
  // Split lines for multiline output
  std::vector<std::string> text_buffer_lines = split(text_buffer, '\n');
  for(unsigned int i = 0,y_new = y;i < text_buffer_lines.size();y_new += font->height(), i++) {
    if (outline_font) {
      rgb_matrix::DrawText(offscreen, *outline_font,
          x - 1, y_new + font->baseline(),
          outline_color, NULL, text_buffer_lines.at(i).c_str(),
          letter_spacing - 2);
      }

      rgb_matrix::DrawText(offscreen, *font, x, y_new + font->baseline(),
            color, NULL, text_buffer_lines.at(i).c_str(),
            letter_spacing);
  }
  // Atomic swap with double buffer
  offscreen = matrix->SwapOnVSync(offscreen);
  return 0;
}

int scroll_text(std::string line, bool *displaymessage, rgb_matrix::Font *outline_font, 
                  rgb_matrix::Font *font, Color outline_color, Color bg_color, Color color,  
                  int letter_spacing, int x_orig, int y_orig, int loops, int displaytime){
  int x = x_orig;
  int y = y_orig;
  int length = 0;
  int delay_speed_usec = (1000000 * displaytime) / (((font->CharacterWidth('W'))+letter_spacing)*line.length() + offscreen->width());
  if (delay_speed_usec < 0) delay_speed_usec = 2000;
  while (!interrupt_received && loops != 0 && *displaymessage) {

    offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);

    if (outline_font) {
      // The outline font, we need to write with a negative (-2) text-spacing,
      // as we want to have the same letter pitch as the regular text that
      // we then write on top.
      rgb_matrix::DrawText(offscreen, *outline_font,
                           x - 1, y + font->baseline(),
                           outline_color, &bg_color,
                           line.c_str(), letter_spacing - 2);
    }

    // length = holds how many pixels our text takes up
    length = rgb_matrix::DrawText(offscreen, *font,
                                  x, y + font->baseline(),
                                  color, outline_font ? NULL : &bg_color,
                                  line.c_str(), letter_spacing);

    if (--x + length < 0) {
      x = x_orig;
      if (loops > 0) --loops;
    }

    usleep(delay_speed_usec);
    // Swap the offscreen_canvas with canvas on vsync, avoids flickering
    offscreen = matrix->SwapOnVSync(offscreen);
  }
  return 0;
}

void paintmode(std::string *pixels, Color *paintcolor, bool *boolsetpixels, bool *paintactive, int *brightness) {
  int ipixels = 0, x = 0, y = 0;
  offscreen->Fill(0,0,0);
  while (*paintactive & !interrupt_received) {
    x = 0;
    y = 0;
    if(*boolsetpixels && !pixels->empty()) {
      for(int i = pixels->length();i > 0;) {
        ipixels = httpserver::hexstrtodecimal(pixels->substr(--i, 1));
        SetPixels(ipixels, *paintcolor, x, y);
        x += 4; // move over 4 bits every iteration (because hex)
        if(x >= offscreen->width()) {
          x = 0;
          y++;
        }
      }
      *boolsetpixels = false;
      matrix->SwapOnVSync(offscreen);
    }
    usleep(10);
    //matrix->SetBrightness(*brightness);
  }
}

int main(int argc, char *argv[]) {
  RGBMatrix::Options matrix_options;
  rgb_matrix::RuntimeOptions runtime_opt;
  if (!rgb_matrix::ParseOptionsFromFlags(&argc, &argv, &matrix_options, &runtime_opt)) {
    return usage(argv[0]);
  }

  const char *time_format = "%H:%M";
  Color color(255, 255, 0);
  Color bg_color(0, 0, 0);
  Color outline_color(0,0,0);
  bool with_outline = false;

  const char *bdf_font_dir = NULL;
  int x_orig = 0;
  int y_orig = 0;
  int brightness = 100;
  int letter_spacing = 0;
  int loops = 1;
  std::string message;
  bool displaymessage = false;
  std::string displaytype;
  int displaytime = 5;
  bool paintactive = false;
  bool boolsetpixels = false;
  std::string pixels;
  Color paintcolor(100,100,100);
  FrameCanvas *tempframe;

  int opt;
  while ((opt = getopt(argc, argv, "x:y:f:C:B:O:b:S:d:l:")) != -1) {
    switch (opt) {
    case 'l': loops = atoi(optarg); break;
    case 'd': time_format = strdup(optarg); break;
    case 'b': brightness = atoi(optarg); break;
    case 'x': x_orig = atoi(optarg); break;
    case 'y': y_orig = atoi(optarg); break;
    case 'f': bdf_font_dir = strdup(optarg); break;
    case 'S': letter_spacing = atoi(optarg); break;
    case 'C':
      if (!parseColor(&color, optarg)) {
        fprintf(stderr, "Invalid color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'B':
      if (!parseColor(&bg_color, optarg)) {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'O':
      if (!parseColor(&outline_color, optarg)) {
        fprintf(stderr, "Invalid outline color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      with_outline = true;
      break;
    default:
      return usage(argv[0]);
    }
  }

  if (bdf_font_dir == NULL) {
    fprintf(stderr, "Need to specify BDF font-dir with -f\n");
    return usage(argv[0]);
  }

  /*
   * Load font. This needs to be a filename with a bdf bitmap font.
   */
  rgb_matrix::Font font;
  if (!font.LoadFont(bdf_font_dir)) {
    fprintf(stderr, "Couldn't load font '%s'\n", bdf_font_dir);
    return 1;
  }
  rgb_matrix::Font *outline_font = NULL;
  if (with_outline) {
      outline_font = font.CreateOutlineFont();
  }

  if (brightness < 1 || brightness > 100) {
    fprintf(stderr, "Brightness is outside usable range.\n");
    return 1;
  }

  matrix = rgb_matrix::CreateMatrixFromOptions(matrix_options,
                                                          runtime_opt);
  if (matrix == NULL)
    return 1;

  matrix->SetBrightness(brightness);

  const bool all_extreme_colors = (brightness == 100)
      && FullSaturation(color)
      && FullSaturation(bg_color)
      && FullSaturation(outline_color);
  if (all_extreme_colors)
      matrix->SetPWMBits(1);

  const int x = x_orig;
  int y = y_orig;

  offscreen = matrix->CreateFrameCanvas();

  char text_buffer[256];
  struct timespec next_time;
  next_time.tv_sec = time(NULL);
  next_time.tv_nsec = 0;
  struct tm tm;

  signal(SIGTERM, InterruptHandler);
  signal(SIGINT, InterruptHandler);
  
  // define and setup variables for passing data from thread to main program.
  LEDsettingsStruct mainsettings;
  mainsettings.textColor = &color;
  mainsettings.bg_color = &bg_color;
  mainsettings.outline_color = &outline_color;
  mainsettings.brightness = &brightness;
  mainsettings.interrupt_received = &interrupt_received;
  mainsettings.message = &message;
  mainsettings.displaymessage = &displaymessage;
  mainsettings.displaymessagetype = &displaytype;
  mainsettings.displaytime = &displaytime;
  mainsettings.paintactive = &paintactive;
  mainsettings.boolsetpixels = &boolsetpixels;
  mainsettings.pixels = &pixels;
  mainsettings.paintcolor = &paintcolor;
  mainsettings.canvaswidth = offscreen->width();
  mainsettings.canvasheight = offscreen->height();

  pthread_t http_thread;

  pthread_create(&http_thread, NULL, http_thread_function, &mainsettings);

  while (!interrupt_received) {
      localtime_r(&next_time.tv_sec, &tm);
      strftime(text_buffer, sizeof(text_buffer), time_format, &tm);
      offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);

      matrix->SetBrightness(brightness);
      //set background color for second ticker
      FillSection((offscreen->width()-60)/2, 0, ((offscreen->width()-60)/2) + 60, 0, outline_color);
      //fill color for seconds that have passed.
      FillSection((offscreen->width()-60)/2, 0, ((offscreen->width()-60)/2) + ((time (NULL)+1) % 60), 0, color);
      //set background color for minutes ticker
      FillSection((offscreen->width()-60)/2, offscreen->height()-1, ((offscreen->width()-60)/2) + 60, offscreen->height()-1, outline_color);
      //fill color for minutes that have passed.
      FillSection((offscreen->width()-60)/2, offscreen->height()-1, ((offscreen->width()-60)/2) + (((time (NULL)+1) % 3600))/60, offscreen->height()-1, color);

      //Display time and date.
      display_text(text_buffer, &font, outline_font, outline_color, bg_color, color, letter_spacing, y, x);

      // Wait until we're ready to show it.
      clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &next_time, NULL);

      if(paintactive) {
          tempframe = matrix->SwapOnVSync(offscreen);
          paintmode(&pixels, &paintcolor, &boolsetpixels, &paintactive, &brightness);
          offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);
          offscreen = matrix->SwapOnVSync(tempframe);
      }

      if(displaymessage && !displaytype.compare("static")) {
          offscreen->Fill(bg_color.r, bg_color.g, bg_color.b);
          int x_pos = 1;
          int y_pos = 0;
          //check if a new line char exists and adjust the text placement
          if(message.find("\n") != std::string::npos) {
              y_pos = (offscreen->height()/4)-font.height()/2;
          }
          else {
              y_pos = (offscreen->height()/2)-font.height()/2;
          }
          display_text(message, &font, outline_font, outline_color, bg_color, color, letter_spacing, y_pos, x_pos);
          usleep(displaytime*1000000);
          displaytype="scroll";
          displaymessage = false;
      }
      else if(displaymessage) {
        scroll_text(message, &displaymessage, outline_font, &font, outline_color, 
                      bg_color, color, letter_spacing, offscreen->width(), (offscreen->height()/2)-font.height()/2, loops, displaytime);
        displaymessage = false;
      }
      next_time.tv_sec += 1;

  }
  pthread_join(http_thread, NULL);
  // Finished. Shut down the RGB matrix.
  matrix->Clear();
  delete matrix;

  write(STDOUT_FILENO, "\n", 1);  // Create a fresh new line after ^C on screen
}
