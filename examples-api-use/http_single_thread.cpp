/* HTTP class for led matrix
 */

#include "led-matrix.h"
#include "graphics.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <string>
#include <stdlib.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <fstream>
#include <mutex> 

#define DEFAULT_SERVER_PORT 5432
#define MAX_PENDING 25
#define MAX_LINE 2048
#define MAX_READ_BYTES 40096
#define SOCKET_READ_TIMEOUT_SEC 1
#define SOCKET_ERROR -1

struct LEDsettingsStruct {
        std::mutex mtx;
        rgb_matrix::Color *textColor;
        rgb_matrix::Color *bg_color;
        rgb_matrix::Color *outline_color;
        std::string *message;
        bool *displaymessage;
        std::string *displaymessagetype;
        int *displaytime;
        int *brightness;
        volatile bool *interrupt_received;
        bool *paintactive;
        bool *boolsetpixels;
        rgb_matrix::Color *paintcolor;
        std::string *pixels;
        int canvaswidth;
        int canvasheight;
  
};


class httpserver {
        private:
                std::string doc_root;
                int serverport;
                LEDsettingsStruct *ledsettings;
        public:
        /* Simple function that allows you to submit a message without
           it being places into a variable */
        int sendString(int new_s, char message[]) {
                send(new_s, message, strlen(message), 0);
                return 0;
        }

        /* Simple function that takes a file path and returns if its
           a regular file or not. (Found online) */
        int isfile(const char *path) {
                struct stat path_stat;
                stat(path, &path_stat);
                return S_ISREG(path_stat.st_mode);
        }

        /* This function takes and returns a string containing the date.
           This function does not crease its own memory allocation */
        char* getcurrentdate(char *c_time_string) {
                c_time_string[0] = '\0';
                time_t rawtime;
                struct tm *info;
                char buffer[80];

                time( &rawtime );

                info = localtime( &rawtime );

                strftime(buffer,80,"%a, %d %b %Y %H:%M:%S %Z", info);
                strcpy(c_time_string, buffer);
                return c_time_string;
        }

        /* This function takes the requested document path and determins its mime type */
        std::string getmimetype(char *requested_doc) {
                char *fileExt = strrchr(requested_doc, '.');
                if(!fileExt || fileExt++ == requested_doc) {
                        return "application/octet-stream";
                }
                else if(strncmp(fileExt, "html", 5) == 0) {
                        return "text/html";
                }
                else if(strncmp(fileExt, "png", 4) == 0) {
                        return "image/png";
                }
                else if(strncmp(fileExt, "jpeg", 5) == 0) {
                        return "image/jpeg";
                }
                else if(strncmp(fileExt, "jpg", 4) == 0) {
                        return "image/jpeg";
                }
                else if(strncmp(fileExt, "css", 4) == 0) {
                        return "text/css";
                }
                return std::string();
        }

        int sendheader(int new_s, char *requested_doc, long contentlength, int statuscode, const char* statusmessage) {
                std::ostringstream header;
                std::string smimetype;
                std::string headerstring;
                char sdate[80];
                if(statuscode != 200) {
                        smimetype = "text/html";
                }
                else {
                        smimetype = getmimetype(requested_doc);
                }
                header << "HTTP/1.0 " << statuscode << " " << statusmessage << "\r\nDate: " << getcurrentdate(sdate) << "\r\nServer: BobsDiscountServer/2.1\r\nContent-Type: "
                        << smimetype << "\r\nContent-Length: " << (int)contentlength << "\r\n\r\n";

                send(new_s, header.str().c_str(), strlen(header.str().c_str()), 0);
                return 0;
        }

        /* This function takes a pointer to a string and
           converts the ascii tags located requested document
           to current set values.
           Reterns the total number of edits done to the string.
           
        */
        int parsetags(std::string *s) {
                int total = 0;
                total += replaceAll(s, "[BRIGHTNESS]", std::to_string((int)*(ledsettings->brightness)));
                total += replaceAll(s, "[TEXTCOLORRED]", std::to_string((int)ledsettings->textColor->r));
                total += replaceAll(s, "[TEXTCOLORGREEN]", std::to_string((int)ledsettings->textColor->g));
                total += replaceAll(s, "[TEXTCOLORBLUE]", std::to_string((int)ledsettings->textColor->b));
                total += replaceAll(s, "[BACKGROUNDCOLORRED]", std::to_string((int)ledsettings->bg_color->r));
                total += replaceAll(s, "[BACKGROUNDCOLORGREEN]", std::to_string((int)ledsettings->bg_color->g));
                total += replaceAll(s, "[BACKGROUNDCOLORBLUE]", std::to_string((int)ledsettings->bg_color->b));
                total += replaceAll(s, "[OUTLINECOLORRED]", std::to_string((int)ledsettings->outline_color->r));
                total += replaceAll(s, "[OUTLINECOLORGREEN]", std::to_string((int)ledsettings->outline_color->g));
                total += replaceAll(s, "[OUTLINECOLORBLUE]", std::to_string((int)ledsettings->outline_color->b));
                total += replaceAll(s, "[OUTLINECOLORBLUE]", std::to_string((int)ledsettings->outline_color->b));
                total += replaceAll(s, "[TEXTHEXCOLOR]", convertRGBtohexstr(ledsettings->textColor->r,
                                        ledsettings->textColor->g,ledsettings->textColor->b));
                total += replaceAll(s, "[BACKGROUNDHEXCOLOR]", convertRGBtohexstr(ledsettings->bg_color->r,
                                        ledsettings->bg_color->g,ledsettings->bg_color->b));
                total += replaceAll(s, "[OUTLINEHEXCOLOR]", convertRGBtohexstr(ledsettings->outline_color->r,
                                        ledsettings->outline_color->g,ledsettings->outline_color->b));
                total += replaceAll(s, "[MESSAGE]", *ledsettings->message);
		total += replaceAll(s, "[DISPLAYMESSAGE]", (*ledsettings->displaymessage ? "true" : "false"));
                total += replaceAll(s, "[CANVASWIDTH]", std::to_string(ledsettings->canvaswidth));
                total += replaceAll(s, "[CANVASHEIGHT]", std::to_string(ledsettings->canvasheight));
                return total;

        }

        /* This function takes a file pointer of a client socket
           and a requested document. The requested document is processed
           through pasetags and sends the retulting file to the clients
           file pointer.
        */
        int Xsendfile(int new_s, char *requested_doc) {
                std::ifstream infile(requested_doc);
                std::string stringbuffer;
                std::string outputstring;
                char buffer[MAX_READ_BYTES+30];
                int len;
                if(!isfile(requested_doc)||!infile.good()) {
                        sendheader(new_s, requested_doc, 20, 404, "File Not Found");
                        sendString(new_s, (char *)"404: File Not Found\n");
                        return 0;
                }

                memset(buffer, 0, (MAX_READ_BYTES+30) * (sizeof (buffer[0]))); // clear buffer

                /* Get file size */
                while((len = infile.readsome(buffer, MAX_READ_BYTES))) {
                        stringbuffer = buffer;
                        outputstring.append(stringbuffer);
                        memset(buffer, 0, MAX_READ_BYTES * (sizeof (buffer[0]))); // clear buffer
                }

                parsetags(&outputstring);
                /* Constructing and sending header */
                sendheader(new_s, requested_doc, (long)outputstring.length(), 200, "OK");

                /* Sending File */
                std::istringstream outdata(outputstring);
                while((len = outdata.readsome(buffer, MAX_READ_BYTES))) {
                        stringbuffer = buffer;
                        send (new_s, stringbuffer.c_str(), len, 0);
                        memset(buffer, 0, MAX_READ_BYTES * (sizeof (buffer[0]))); // clear buffer
                }

                /* Cleaning Up */
                infile.close();
                return 1;
        }

        std::string getrequest(int new_s) {
                int len;
                char buf[MAX_LINE+30];
                std::string request;
                size_t pos;
                int requestlen = 0;
                int requestnumber = 0;
                for(int i = 0;i < MAX_LINE+30;request[i++]='\0');
                while((len = recv(new_s, buf, sizeof(buf), 0))) {

                        requestlen += len;
                        requestnumber++;
                        //if client disconnects
                        if(requestlen >= MAX_LINE || len == 0) {
                                return std::string();
                        }

                        if(requestnumber == 1) {
                                request.append(buf);
                        }
                        else {
                                request.append(buf);
                        }
                        if((pos = request.find("\r\n\r\n")) != std::string::npos) {
                                break;
                        }
                }
                return request.substr(0, pos);
        }

        /* This function takes in the full document request and finds/returns the clients set var.
           example: getURLValue("/?BRIGHTNESS=100", "BRIGHTNESS") RETURNS 100 as a string.
        */
        std::string getURLValue(std::string request_doc, std::string reqestedVar) {
                size_t pos;
                std::string settingstring;
                /* Checks for the reqestedVar presents and extracts data.
                   substr takes start position (char) and length. Char length is calculated with 
                   request_doc.find("&", pos+1)-(pos+reqestedVar.length()+2)  
                */
                if((pos = request_doc.find("?" + reqestedVar + "=")) != std::string::npos) {
                        settingstring = request_doc.substr(pos + reqestedVar.length()+2, request_doc.find("&", pos+1)-(pos+reqestedVar.length()+2));
                        decode_request_doc(&settingstring);
                        return settingstring;
                }
                else if((pos = request_doc.find("&" + reqestedVar + "=")) != std::string::npos) {
                        settingstring = request_doc.substr(pos + reqestedVar.length()+2, request_doc.find("&", pos+1)-(pos+reqestedVar.length()+2));
                        decode_request_doc(&settingstring);
                        return settingstring;
                }
                return std::string();
        }

        static int hexstrtodecimal(std::string hexstr) {
                int decimalint;
                std::stringstream ss;
                ss << std::hex << hexstr;
                ss >> decimalint;
                return decimalint;
        }
        std::string decimaltohexstr(int decimalint) {
                std::string hexstr;
                std::stringstream ss;
                ss << std::hex << decimalint;
                ss >> hexstr;
                if(decimalint < 16) {
                        hexstr = "0" + hexstr;
                }
                return hexstr;
        }
        /* This function takes a string in the format of a hex colors and converts
           it to red, green and blue int values
        */
        int parsehexcolor(std::string str, unsigned char *red, unsigned char *green, unsigned char *blue) {
                if(str.at(0) != '#' || str.length() != 7) {
                        return -1;
                }

                std::string hexred = str.substr(1, 2);
                std::string hexgreen = str.substr(3, 2);
                std::string hexblue = str.substr(5, 2);
                
                *red = valueBetween(hexstrtodecimal(hexred), 255, 0);
                *green = valueBetween(hexstrtodecimal(hexgreen), 255, 0);
                *blue = valueBetween(hexstrtodecimal(hexblue), 255, 0);

                return 0;
        }

        /* This function takes a RGB (unsigned chars) and converts it
           hex colors format and returns it as a string.
        */
        std::string convertRGBtohexstr(unsigned char red, unsigned char green, unsigned char blue) {

                std::string hexstr;
                hexstr = "#" + decimaltohexstr((int)red) + decimaltohexstr((int)green) + decimaltohexstr((int)blue);

                return hexstr;
        }

        /* This function takes 3 strings in:
           str - string to be searched and editied
           strfind - string to be found in str
           strreplace - string to replace strfind in str.
        */
        int replaceAll(std::string *str, std::string strfind, std::string strreplace) {
                size_t pos; int numreplaced = 0;
                while((pos = str->find(strfind)) != std::string::npos) {
                        str->replace(pos, strfind.length(), strreplace);
                        numreplaced++;
                }
                return numreplaced;
        }
        /* This function takes the request_doc string
           and the encoded URL to normal chars */
        int decode_request_doc(std::string* request_doc) {
                replaceAll(request_doc, "+", " ");
                // lets do this later
                /*while((pos = str->find("%")) != std::string::npos && pos < request_doc.length() + 1) {
                        str->replace(pos, strfind.length(), strreplace);
                        numreplaced++;
                }*/

                replaceAll(request_doc, "%20", " ");
                replaceAll(request_doc, "%21", "!");
                replaceAll(request_doc, "%22", "\"");
                replaceAll(request_doc, "%23", "#");
                replaceAll(request_doc, "%24", "$");
                replaceAll(request_doc, "%25", "%");
                replaceAll(request_doc, "%26", "&");
                replaceAll(request_doc, "%27", "'");
                replaceAll(request_doc, "%28", "(");
                replaceAll(request_doc, "%29", ")");
                replaceAll(request_doc, "%2A", "*");
                replaceAll(request_doc, "%2C", ",");
                replaceAll(request_doc, "%2D", "-");
                replaceAll(request_doc, "%2E", ".");
                replaceAll(request_doc, "%2F", "/");
                replaceAll(request_doc, "%3A", ":");
                replaceAll(request_doc, "%3B", ";");
                replaceAll(request_doc, "%3D", "=");
                replaceAll(request_doc, "%3E", ">");
                replaceAll(request_doc, "%3C", "<");
                replaceAll(request_doc, "%3F", "?");
                replaceAll(request_doc, "%40", "@");
                replaceAll(request_doc, "%5E", "^");
                replaceAll(request_doc, "%5C", "\\");
                replaceAll(request_doc, "%5D", "]");
                replaceAll(request_doc, "%5D", "[");
                replaceAll(request_doc, "%60", "`");
                replaceAll(request_doc, "%7B", "{");
                replaceAll(request_doc, "%7C", "|");
                replaceAll(request_doc, "%7D", "}");
                replaceAll(request_doc, "%7E", "~");

                return 0;
        }
        /* This function takes three int values: val, max, min
           and returns a valid RGB value. If colorval > max return max
           if colorval < min return min;
         */
        int valueBetween(int val, int max, int min){
                if(val > max) {
                        return max;
                }
                if(val < min) {
                        return min;
                }
                return val;
        }

        int processledinputs(std::string request_doc) {
                std::string BRIGHTNESS = getURLValue(request_doc, "BRIGHTNESS");
                std::string TEXTCOLOR = getURLValue(request_doc, "TEXTCOLOR");
                std::string COLORRED = getURLValue(request_doc, "COLORRED");
                std::string COLORGREEN = getURLValue(request_doc, "COLORGREEN");
                std::string COLORBLUE = getURLValue(request_doc, "COLORBLUE");
                std::string BACKGROUNDRED = getURLValue(request_doc, "BACKGROUNDRED");
                std::string BACKGROUNDGREEN = getURLValue(request_doc, "BACKGROUNDGREEN");
                std::string BACKGROUNDBLUE = getURLValue(request_doc, "BACKGROUNDBLUE");
                std::string BACKGROUNDCOLOR = getURLValue(request_doc, "BACKGROUNDCOLOR");
                std::string OUTLINERED = getURLValue(request_doc, "OUTLINERED");
                std::string OUTLINEGREEN = getURLValue(request_doc, "OUTLINEGREEN");
                std::string OUTLINEBLUE = getURLValue(request_doc, "OUTLINEBLUE");
                std::string OUTLINECOLOR = getURLValue(request_doc, "OUTLINECOLOR");
                std::string MESSAGE = getURLValue(request_doc, "MESSAGE");
                std::string DISPLAYMESSAGE = getURLValue(request_doc, "DISPLAYMESSAGE");
                std::string DISPLAYMESSAGETYPE = getURLValue(request_doc, "DISPLAYMESSAGETYPE");
                std::string DISPLAYMESSAGETIME = getURLValue(request_doc, "DISPLAYMESSAGETIME");
                std::string PAINTACTIVE = getURLValue(request_doc, "PAINTACTIVE");
                std::string SETPIXELS = getURLValue(request_doc, "SETPIXELS");
                std::string COLORPAINTPIXELS = getURLValue(request_doc, "COLORPAINTPIXELS");

                ledsettings->mtx.lock();
                // LED pannel brightness
                if(!BRIGHTNESS.empty() && BRIGHTNESS.length() <= 3) {
                        *(ledsettings->brightness) = valueBetween(atoi(BRIGHTNESS.c_str()), 100, 0);
                }
                // LED text color html5
                if(!TEXTCOLOR.empty() && TEXTCOLOR.length()  == 7) {
                        parsehexcolor(TEXTCOLOR, &ledsettings->textColor->r, 
                                &ledsettings->textColor->g, &ledsettings->textColor->b);
                }
                // Time text color RED
                if(!COLORRED.empty() && COLORRED.length() <= 3) {
                        ledsettings->textColor->r = valueBetween(atoi(COLORRED.c_str()), 255, 0);
                }
                // Time text color GREEN
                if(!COLORGREEN.empty() && COLORGREEN.length() <= 3) {
                        ledsettings->textColor->g = valueBetween(atoi(COLORGREEN.c_str()), 255, 0);
                }
                // Time text color BLUE
                if(!COLORBLUE.empty() && COLORBLUE.length() <= 3) {
                        ledsettings->textColor->b = valueBetween(atoi(COLORBLUE.c_str()), 255, 0);
                }
                // LED background color html5
                if(!BACKGROUNDCOLOR.empty() && BACKGROUNDCOLOR.length()  == 7) {
                        parsehexcolor(BACKGROUNDCOLOR, &ledsettings->bg_color->r, 
                                &ledsettings->bg_color->g, &ledsettings->bg_color->b);
                }
                // LED background RED
                if(!BACKGROUNDRED.empty() && BACKGROUNDRED.length() <= 3) {
                        ledsettings->bg_color->r = valueBetween(atoi(BACKGROUNDRED.c_str()), 255, 0);
                }
                // LED background GREENSOCKET_READ_TIMEOUT_SEC
                if(!BACKGROUNDGREEN.empty() && BACKGROUNDGREEN.length() <= 3) {
                        ledsettings->bg_color->g = valueBetween(atoi(BACKGROUNDGREEN.c_str()), 255, 0);
                }
                // LED background BLUE
                if(!BACKGROUNDBLUE.empty() && BACKGROUNDBLUE.length() <= 3) {
                        ledsettings->bg_color->b = valueBetween(atoi(BACKGROUNDBLUE.c_str()), 255, 0);
                }
                // LED outline color html5
                if(!OUTLINECOLOR.empty() && OUTLINECOLOR.length()  == 7) {
                        parsehexcolor(OUTLINECOLOR, &ledsettings->outline_color->r, 
                                &ledsettings->outline_color->g, &ledsettings->outline_color->b);
                }
                // LED outline RED
                if(!OUTLINERED.empty() && OUTLINERED.length() <= 3) {
                        ledsettings->outline_color->r = valueBetween(atoi(OUTLINERED.c_str()), 255, 0);
                }
                // LED outline GREEN
                if(!OUTLINEGREEN.empty() && OUTLINEGREEN.length() <= 3) {
                        ledsettings->outline_color->g = valueBetween(atoi(OUTLINEGREEN.c_str()), 255, 0);
                }
                // LED outline BLUE
                if(!OUTLINEBLUE.empty() && OUTLINEBLUE.length() <= 3) {
                        ledsettings->outline_color->b = valueBetween(atoi(OUTLINEBLUE.c_str()), 255, 0);
                }
                // Get message send to led panel
                if(!MESSAGE.empty()) {
                        replaceAll(&MESSAGE, "%n", "\n");
                        *ledsettings->message = MESSAGE;
                }
                /* Get display message type to determin how to display the message on the led pannel.
                   currently only scroll and static is available. Scroll is default and if the command
                   is not understood it will default to scrolling text */
                if(!DISPLAYMESSAGETYPE.empty()) {
                        *ledsettings->displaymessagetype = DISPLAYMESSAGETYPE;
                }
                // Set how long the message will be displayed on the screen.
                if(!DISPLAYMESSAGETIME.empty()) {
                        // how long the message will be displayed on the screen. In seconds.
                        *ledsettings->displaytime = valueBetween(atoi(DISPLAYMESSAGETIME.c_str()), 100, 1);
                }
                // set display messages status (case sensitive). can be used to cut message short
                if(!DISPLAYMESSAGE.empty()) {
                        *ledsettings->displaymessage = (!DISPLAYMESSAGE.compare("true") ? true : false);
                }
                // set panel to externally manipulated
                if(!PAINTACTIVE.empty()) {
                        *ledsettings->paintactive = (!PAINTACTIVE.compare("true") ? true : false);
                }
                // set true to signal new pattern to be drawn
                if(!SETPIXELS.empty()) {
                        *ledsettings->boolsetpixels = (!SETPIXELS.compare("true") ? true : false);
                }
                // set individual pixels with the first 6 hex numbers being RGB color
                /* debugging note: to use this in a web browser submit "%23" instead of "#" (You're welcome :D) */
                if(!COLORPAINTPIXELS.empty() && COLORPAINTPIXELS.length() >= 8 && COLORPAINTPIXELS.find("#") != std::string::npos) {
                        parsehexcolor(COLORPAINTPIXELS.substr(COLORPAINTPIXELS.find("#")), &ledsettings->paintcolor->r, 
                                &ledsettings->paintcolor->g, &ledsettings->paintcolor->b);
                        *ledsettings->pixels = COLORPAINTPIXELS.substr(0, COLORPAINTPIXELS.find("#"));
                }
                ledsettings->mtx.unlock();

                return 0;
        }

        int handleclient(int clientsocket) {
                std::string request_type;
                std::string request_doc;
                std::string request_protocol;
                std::string settingstring;
                std::string request = getrequest(clientsocket);
                std::string saveptr;
		std::string file;
                size_t pos;


                if(!request.empty() && (pos = request.find(" ")) != std::string::npos) {
                        request_type = request.substr(0, pos);
                        request_doc = request.substr(pos+1, request.find(" ", pos+1)-(pos+1));
                        //request_protocol = request.substr(request.rfind(" ")+1, request.find("\n")-2);
                }
                if(request.empty()) {
                        sendheader(clientsocket, NULL, 30, 413, (char *)"Request Entity Too Large");
                        sendString(clientsocket, (char *)"413: Request Entity Too Large\n");
                        close(clientsocket);
                }
                else if(request_type.compare(0, 4,"GET") != 0) {
                        sendheader(clientsocket, NULL, 17, 400, (char *)"Bad Request");
                        sendString(clientsocket, (char *)"400: Bad Request");
                }
                else if(request_doc.length() < 1) {
                        sendheader(clientsocket, NULL, 17, 400, (char *)"Bad Request");
                        sendString(clientsocket, (char *)"400: Bad Request\n");
                }
                else if(request_doc.at(0) != '/') {
                        sendheader(clientsocket, NULL, 17, 400, (char *)"Bad Request");
                        sendString(clientsocket, (char *)"400: Bad Request\n");
                }
                else if (request_doc.find("..") != std::string::npos) {
                        sendheader(clientsocket, NULL, 15, 403, (char *)"Forbidden");
                        sendString(clientsocket, (char *)"403: Forbidden\n");
                }
		else if(!request_doc.compare("/getsettings.html"))
		{
			file = this->doc_root + "getsettings.html";
                        Xsendfile(clientsocket, (char*)file.c_str());
		}
                else {
                        // process http get input and set led panel
                        processledinputs(request_doc);

                        // send index.html file.
                        file = this->doc_root + "index.html";
                        Xsendfile(clientsocket, (char*)file.c_str());
                }
                close(clientsocket);
                return 0;

        }

        int runserver() {

                struct sockaddr_in sin;
                socklen_t len;
                int s, new_s;
                int enable = 1;
                /* build address data structure */
                memset((char *)&sin, 0, sizeof(sin));
                sin.sin_family = AF_INET;
                sin.sin_addr.s_addr = inet_addr("127.0.0.1");
                sin.sin_port = htons(serverport);
                /* setup passive open */
                if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
                        perror("simplex-talk: socket");
                        exit(1);
                }
                
                fcntl(s, F_SETFL, O_NONBLOCK);  // set to non-blocking
                
                /* SO_REUSEADDR allows for immediate rebinding to ports after close */
                if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) 
                        perror("setsockopt(SO_REUSEADDR) failed");
                if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
                        perror("simplex-talk: bind");
                        exit(1);
                }
                listen(s, MAX_PENDING);

                fd_set set;
                struct timeval timeout;

                printf("Server Listening on Port %i\n", serverport);

                while(true) {
                        FD_ZERO(&set); /* clear the set */
                        FD_SET(s, &set); /* add our file descriptor to the set */
                        timeout.tv_sec = SOCKET_READ_TIMEOUT_SEC;
                        timeout.tv_usec = 0;
                        if((new_s = select(s+1, &set, NULL, NULL, &timeout)) == SOCKET_ERROR) {
                                perror("simplex-talk: accept");
                        }
                        else if(*ledsettings->interrupt_received) {
                                close(s);
                                close(new_s);
                                pthread_exit(NULL);
                        }
                        else if(new_s == 0) {
                        }
                        else if ((new_s = accept(s, (struct sockaddr *)&sin, &len)) < 0) {
                                perror("simplex-talk: accept");
                        }
                        else {
                                handleclient(new_s);
                        }
                }
        }

        /* Constructors for defaults */
        httpserver(int serverport, char doc_root[], struct LEDsettingsStruct *ledsettings){
                this->doc_root = doc_root;
                this->serverport = serverport;
                this->ledsettings = ledsettings;
        }

};
