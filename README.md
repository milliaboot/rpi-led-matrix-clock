# rpi led matrix clock

This is a fork of [rpi-rgb-led-matrix](https://github.com/hzeller/rpi-rgb-led-matrix).
Added features include http interface to display text and paintmode to allow external modification of the display without shutting down the clock